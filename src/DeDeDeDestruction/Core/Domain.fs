namespace DeDeDeDestruction.Core

open System

module Domain =

  type ContactIdentif =
    | PhoneNumber of string
    | EMail       of string

  type User =
    { UserId   : Guid
      UserName : string
      Contacts : ContactIdentif list }

  type Item =
    { ItemId : Guid
      ItemName : string }

  type Project =
    { ProjectId      : Guid
      OrganisationId : Guid
      ProjectName    : string
      Manager        : Guid
      CreatedAt      : int64
      Members        : Guid list }

  type Organisation =
    { OrganisationId   : Guid
      OrganisationName : string }

  type UserProfile =
    { UserProfileId : Guid
      UserProfileName : string }

  type Site =
    { UserProfiles : UserProfile list }
